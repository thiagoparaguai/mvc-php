<?php
require 'config.php';
// Autoload de carregamento
spl_autoload_register(function ($class) {
	
	if(strpos($class, 'Controller') > -1):
		if(file_exists('controllers/'.$class.'.php')):
			require_once 'controllers/'.$class.'.php';
		endif;
	elseif(file_exists('models/'.$class.'.php')):
		require_once 'models/'.$class.'.php';
		else:
			require_once 'core/'.$class.'.php';
	endif;
	
});

$core = new Core();
$core->run();


?>
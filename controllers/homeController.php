<?php
class homeController extends controller {
	
	public function index() {
		$produtos = new Produtos();
		$dados['produtos'] = $produtos->getProdutos();
				
		// Chamando a loadView
		$this->loadTemplate('home', $dados);
	}
	
	public function sobre() {
		$dados = array();
		$this->loadTemplate('sobre', $dados);
	}
}
<?php
class Produtos extends Model {
	
	public function getProdutos() {
		$array =  array();
		
		$sql = "SELECT * FROM produtos";
		$sql = $this->db->query($sql);
		
		if($sql->rowCount() > 0):
			$array = $sql->fetchAll();
		endif;
		
		return $array;
	}
	
}
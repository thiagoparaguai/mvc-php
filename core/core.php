<?php
class Core {
	
	// Método principal
	public function run() {
		// Pega a url
		$url = explode("index.php", $_SERVER['PHP_SELF']);
		$url = end($url);
		
		// Array da url com action vazia
		$params = array();
		
		if(!empty($url)):
			$url = explode('/',$url);
			// Array_shift remove a primeira chave do array
			array_shift($url);

			$currentController = $url[0].'Controller';
			// Array_shift tira o controller current do array
			array_shift($url);
			
			if(isset($url[0])):
				$currentAction = $url[0];
				array_shift($url);
				else:
					$currentAction = 'index';
			endif;
			
			/* Depois de pegar o controller e action
			e excluir todos, após ira pegar o array
			com parametros */
			
			
			/* Se existir mais algum registro e for maio que 0
			vamos amarzenar em uma variavel */
			if(count($url) > 0 ):
				$params = $url;
			endif;
			
		else:
			$currentController = 'homeController';
			$currentAction     = 'index';
		endif;
		
		// require_once no controller base
		require_once 'core/controller.php';
		
		/* // Verificando paramentros
		echo "CONTROLLER: ".$currentController."</br>";
		echo "<br/>Action: ".$currentAction."</br>";
		print_r($params);
		// Exit para para a execução do codigo aqui
		exit; */
		
		$c = new $currentController();
		call_user_func_array(array($c, $currentAction), $params);
		
/* 		echo "CONTROLLER: ".$currentController;
		echo "<br/>Action: ".$currentAction; */
	}
	
}